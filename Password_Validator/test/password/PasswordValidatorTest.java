package password;
/*
 * 
 * @author Robin Lansiquot 
 * 
 * Assumption is that spaces are not considered valid characterss
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {
	
<<<<<<< HEAD
	
	
	@Test 
	public void testHasValidCaseCharsRegular () {
		boolean result = PasswordValidator.hasValidCaseChars("aAaAaAaA");
		assertTrue ("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryInLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aA");
		assertTrue ("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("     ");
		assertFalse ("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse ("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		boolean result = PasswordValidator.hasValidCaseChars("8989898");
		assertFalse ("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAA");
		assertFalse ("Invalid case characters", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaaa");
		assertFalse ("Invalid case characters", result);
	}
	
	
	
	//Testing has hasEnoughDigits()
=======
	//*******************Testing has hasEnoughDigits()*******************//
>>>>>>> refs/heads/master
	
	@Test
	public void testhasEnoughDigitsRegular() {
		assertTrue("Invalid numbers of digits", PasswordValidator.hasEnoughDigits("abcdefgh1234"));
	}
	
	@Test
	public void testHasEnoughDigitsException() {
		assertFalse("Invalid numbers of digits", PasswordValidator.hasEnoughDigits("abcdefgh"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryin() {
		assertTrue("Invalid number of digits", PasswordValidator.hasEnoughDigits("abcdefgh12"));
	}
	
	@Test
	public void testHasEnoughDigitsBoundaryOut() {
		assertFalse("Invalid number of digits", PasswordValidator.hasEnoughDigits("abcdefgh1"));
	}
	
	

	
	
	
	
	
	
	
	//*******************Testing isValidLength ()*******************//
	
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("1234567890"));
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid password length", PasswordValidator.isValidLength("12345678"));
	}
	
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("1234567"));
	}
	
	@Test
	public void testIsValidLengthBoundaryException() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength(null));
	}
	
	@Test
	public void testIsValidLengthBoundaryExceptionSpaces() {
		assertFalse("Invalid password length", PasswordValidator.isValidLength("            "));
	}


}
