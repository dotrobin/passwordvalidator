package password;

/*
 * 
 * @author Robin Lansiquot 
 * 
 * This class validates passwords and it will be developed using TDD
 */


public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	private static int MIN_NUM_DIGITS = 2;
	
	public static boolean hasValidCaseChars (String password) {
		return password != null && password.matches(".*[A-Z]+.*") &&  password.matches(".*[a-z]+.*");
	}
	
	//Password must have valid length
	public static boolean isValidLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}
		return false;
		
	}
	
	public static boolean hasEnoughDigits(String password) {
		
		int digits = 0;
		
		for (int i = 0;i<password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				digits++;
				
				if(digits >= MIN_NUM_DIGITS) {
					return true;
				}
			}
		}
		
		return false;
		
	}
		
		
}


